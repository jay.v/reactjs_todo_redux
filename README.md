# ReactJs Redux TODO APP
A ReactJs Redux based Todos App, this uses Browser's local storage for Persisting the todos list storage.

## Styles
- CSS
## Components
-  Delete All Button
- Input Text Field
- Todo
- Todo List

## Redux Action Types
- PERSIST_TODOS
- ADD_CURRENT_TODO_TEXT
- ADD_TODO
- DELETE_TODO
- EDIT_TODO
- COMPLETE_EDIT_TODO
- DELETE_ALL_TODO
