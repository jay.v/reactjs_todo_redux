import { useEffect } from 'react';
import { connect } from 'react-redux';
import { persistTodos } from './redux/action/todo.action.js';

import InputTextField from './components/InputTextField/InputTextField.jsx';
import TodosList from './components/TodoList/TodosList.jsx';
import DeleteAllTodosButton from './components/DeleteAllTodosButton/DeleteAllTodosButton.jsx';


import './App.css';

function App({ persistLocalStorageReduxHelper }) {
  useEffect(() => {
    persistLocalStorageReduxHelper();
  }, [persistLocalStorageReduxHelper])
  return (
    <div className="App">
      <h1>TODOs ReactJs Redux APP</h1>
      <InputTextField />
      <TodosList />
      <DeleteAllTodosButton />
    </div>
  );
}

const mapDispatchToProps = dispatch => ({
  persistLocalStorageReduxHelper: () => dispatch(persistTodos())
})

export default connect(null, mapDispatchToProps)(App);
