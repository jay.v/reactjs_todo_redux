import React from 'react'
import { connect } from 'react-redux'
import { deleteAllTodos } from '../../redux/action/todo.action'

import './deleteAllTodosButton.css'

function DeleteAllTodosButton({ todos, deleteAllTodosReduxHelper }) {

    const handleOnDeleteAllClicked = () => {
        deleteAllTodosReduxHelper();
    }

    return (
        <div>
            {todos.length > 1 ? <button className="delete_button" onClick={handleOnDeleteAllClicked}>Delete All Todos</button> : null}
        </div>
    )
}

const mapStateToProps = state => ({
    todos: state.todos
})
const mapDispatchToProps = dispatch => ({
    deleteAllTodosReduxHelper: () => dispatch(deleteAllTodos())
})

export default connect(mapStateToProps, mapDispatchToProps)(DeleteAllTodosButton)
