import React from 'react'
import { connect } from 'react-redux';
import { addCurrentTodoText, addTodo, completeEditTodo } from '../../redux/action/todo.action';

import './inputTextField.css'

function InputTextField({ editSelected, currentTodoText, addTodoReduxHelper, addCurrentTodoTextReduxHelper, completeEditTodoReduxHelper }) {


    const handleTodoChange = e => {
        addCurrentTodoTextReduxHelper(e.target.value);
    }

    const handleFormSubmit = e => {
        e.preventDefault();

        if (editSelected !== undefined) {
            completeEditTodoReduxHelper(currentTodoText, editSelected)
        }
        else {
            addTodoReduxHelper(currentTodoText);
        }
    }


    return (
        <form onSubmit={handleFormSubmit}>
            <input className="input_field" value={currentTodoText} type="text" name="todo" placeholder="Enter Your Todo..." onChange={handleTodoChange} />
        </form>
    )
}

const mapDispatchToProps = dispatch => ({
    addTodoReduxHelper: todo => dispatch(addTodo(todo)),
    addCurrentTodoTextReduxHelper: todoText => dispatch(addCurrentTodoText(todoText)),
    completeEditTodoReduxHelper: (value, todoId) => dispatch(completeEditTodo(value, todoId))
})

const mapStateToProps = state => ({
    currentTodoText: state.currentText,
    editSelected: state.editSelected
})

export default connect(mapStateToProps, mapDispatchToProps)(InputTextField)
