import React from 'react'
import { connect } from 'react-redux'
import { deleteTodo, editTodo } from '../../redux/action/todo.action'

import './todos.css'

function Todo({ todo, todoId, editSelected, currentText, deleteTodoReduxHelper, editTodoReduxHelper }) {

    const handleDeleteKeyPressed = () => {
        deleteTodoReduxHelper(todoId);
    }

    const handleOnTodoClicked = () => {
        editTodoReduxHelper(todoId)
    }

    return (
        <div className="todo">
            <div className="pointer_cursor" onClick={handleOnTodoClicked}>{

                editSelected === todoId ? currentText : todo
            }</div>
            <div className="delete_todo_key" onClick={handleDeleteKeyPressed}>X</div>
        </div>
    )
}

const mapDispatchToProps = dispatch => ({
    deleteTodoReduxHelper: key => dispatch(deleteTodo(key)),
    editTodoReduxHelper: key => dispatch(editTodo(key))
})

const mapStateToProps = state => ({
    editSelected: state.editSelected,
    currentText: state.currentText
})

export default connect(mapStateToProps, mapDispatchToProps)(Todo)
