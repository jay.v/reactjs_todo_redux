import React from 'react'
import { connect } from 'react-redux'
import Todo from './Todo.jsx'

import './todos.css'

function TodosList({ todos }) {
    return (
        <div>
            <h2>Existing Todos</h2>
            {
                todos.length > 0 ? <div className="todo_list_wrap">
                    {
                        todos.map((todo, i) => (
                            <Todo key={i} todo={todo} todoId={i} />
                        ))
                    }
                </div> :
                    <p>No Todos Yet!</p>
            }
        </div>
    )
}

const mapStateToProps = state => ({
    todos: state.todos
})

export default connect(mapStateToProps)(TodosList)
