import * as actionTypes from "../types";

export const persistTodos = () => ({
    type: actionTypes.PERSIST_TODOS
})
export const addCurrentTodoText = todoText => ({
    type: actionTypes.ADD_CURRENT_TODO_TEXT,
    payload: todoText
})
export const addTodo = todo => ({
    type: actionTypes.ADD_TODO,
    payload: todo
})
export const deleteTodo = todoKey => ({
    type: actionTypes.DELETE_TODO,
    payload: todoKey
})
export const editTodo = todoKey => ({
    type: actionTypes.EDIT_TODO,
    payload: todoKey
})
export const completeEditTodo = (value, todoId) => ({
    type: actionTypes.COMPLETE_EDIT_TODO,
    payload: {
        value: value,
        todoId: todoId
    }
})
export const deleteAllTodos = () => ({
    type: actionTypes.DELETE_ALL_TODO
})
