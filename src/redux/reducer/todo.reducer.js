import * as actionTypes from "../types";
const initState = {
    todos: [],
    currentText: '',
    editSelected: undefined
}

const localStorageTodoKey = 'todos_local_storage';
let todosTemp = [];

const setPersistStorage = (todos) => {
    window.localStorage.setItem(localStorageTodoKey, JSON.stringify(todos));
}

export const todoReducer = (state = initState, action) => {
    console.log(action)
    switch (action.type) {
        case actionTypes.PERSIST_TODOS:
            todosTemp = window.localStorage.getItem(localStorageTodoKey)
            todosTemp = todosTemp ? JSON.parse(todosTemp) : []
            return { ...state, todos: todosTemp }
        case actionTypes.ADD_CURRENT_TODO_TEXT:
            return { ...state, currentText: action.payload }
        case actionTypes.ADD_TODO:
            todosTemp = state.todos.concat(action.payload)
            setPersistStorage(todosTemp)
            return { ...state, todos: todosTemp, currentText: '' }
        case actionTypes.DELETE_TODO:
            todosTemp = state.todos.filter((todo, i) => i !== action.payload)
            setPersistStorage(todosTemp)
            return { ...state, todos: todosTemp }
        case actionTypes.EDIT_TODO:
            return { ...state, currentText: state.todos[action.payload], editSelected: action.payload }
        case actionTypes.COMPLETE_EDIT_TODO:
            todosTemp = state.todos.map((todo, i) => i !== action.payload.todoId ? todo : action.payload.value)
            setPersistStorage(todosTemp)
            return { ...state, todos: todosTemp, currentText: '', editSelected: undefined }
        case actionTypes.DELETE_ALL_TODO:
            setPersistStorage([])
            return { ...state, todos: [], currentText: '', editSelected: undefined }
        default:
            return state;
    }
}
